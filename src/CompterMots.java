public class CompterMots {
    private boolean containsChar(String s, char search) {
        if (s.length() == 0)
            return false;
        else
            return s.charAt(0) == search || containsChar(s.substring(1),search);
    }

    public Integer compterMots(String phrase){
        String asciiSpace = "\t\n\b\r ";
        Integer nb = 0;
        Integer i = 0;
        Integer motStart = 0;
        while ((i<phrase.length()) && (containsChar(asciiSpace,phrase.charAt(i)))) {
            i++;
        }
        while (i<phrase.length()) {
            if (!containsChar(asciiSpace,phrase.charAt(i))) {
                i++;
                continue;
            }
            nb++;
            i++;
            while ((i<phrase.length()) && (containsChar(asciiSpace,phrase.charAt(i)))) {
                i++;
            }
            motStart = i;
        }
        if (motStart < phrase.length()) {
            nb++;
        }
        return nb;
    }
}
